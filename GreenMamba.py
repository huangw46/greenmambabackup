####################################################################################################################################
#
# GreenMamba - GUI for iCER
# Authors: Brian Lowen, Grant Schonhoff, Griffin Carr, Jaiwant Bhushan, Will Giger, Weiyu Huang, and Cody Lowen
# Sources Used: https://python-gtk-3-tutorial.readthedocs.io/en/latest/
#               https://www.programcreek.com/python/example/88440/gi.repository.Gtk.CssProvider
#               https://developer.gnome.org/gnome-devel-demos/stable/treeview_simple_liststore.py.html.en
#               https://unix.stackexchange.com/questions/190495/how-to-execute-a-bash-command-in-a-python-script
#               https://developer.gnome.org/gnome-devel-demos/stable/image.py.html.en
#               https://stackoverflow.com/questions/45162862/how-do-i-set-an-icon-for-the-whole-application-using-pygobject
#               https://lazka.github.io/pgi-docs/Gtk-3.0/classes.html
#               https://coolsymbol.com/
#               https://stackoverflow.com/questions/53678686/python-gtk-3-how-to-center-text-within-combobox-widget
# Version: 3.0
####################################################################################################################################
import os, signal
import gi
import subprocess
import shlex
import queue
import time
import threading


import sys
from threading import Thread
from multiprocessing import Process, Queue, Pool

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Pango, GObject, GLib, GdkPixbuf

class MainWindow(Gtk.Window):
    '''
    Class for the main window
    '''

    def __init__(self):
        self.task_list = {"node_window":[], "job_window": []}
        self.node_queue = None
        self.job_queue = None
        self.timer = time.time()
        self.node_window = None
        self.job_window = None
        self.process_id = None

        # Window and grid creation
        Gtk.Window.__init__(self, title="Grid")
        self.set_default_size(100, 100)

        # initialize and add a grid to the window
        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.contact_label = ""

        self.processList = []

        # import image from images folder
        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file("/opt/software/GUI/GreenMambaGUI/images/icer_icon.png")
            self.set_default_icon(pixbuf)
        except:
            print("Failed to load icon")

        # Gets the users groups and quota
        self.groups = self.get_groups()
        self.quota = self.get_quota()
        self.status = self.get_current_status()
        self.buyinAccount = self.getBuyin()

        self.homeInfo = " " + self.groups + "\n\n " + self.quota + "\n\n " + self.status

        if self.buyinAccount:
            self.homeInfo += "\n\n " + "<b>Buy-in Account: </b> \n     " + self.buyinAccount

        self.home_window_open = False
        self.support_window_open = False
        self.status_clickable = True
        self.buyin_clickable = True

        # Currently active window we'll place on the grid. (Not actually a window, just the current item placed on the grid)
        self.activeWindow = Gtk.Label(label="")

        self.activeSort = ""

        # Header creation
        hb = Gtk.HeaderBar()

        # Header title (Application name + Current User)
        self.title = "{:^13s}\n".format("GreenMamba") + "{:^13s}".format("User: " + os.environ['USER'])
        hb.set_decoration_layout(None)
        hb.props.custom_title = Gtk.Label(label=self.title)
        self.set_titlebar(hb)
        hb.set_name("hb")

        # Button creation
        self.button_status = Gtk.Button(label="  Nodes     ")
        self.button_home = Gtk.Button(label="    Home     ➤")
        self.button_support = Gtk.Button(label="    Support  ➤")
        self.button_exit = Gtk.Button(label=" Exit   ")

        self.button_exit.set_name("exit")

        # Button signals which allow them to connect to functions when clicked
        self.button_status.connect("clicked", self.windowSetUp, "node_window")
        self.button_home.connect("clicked", self.homeClick)
        self.button_support.connect("clicked", self.supportClick)
        self.button_exit.connect("clicked", self.exitClick)

        #
        # Buyin Node Box
        #
        buyin_store = Gtk.ListStore(str)
        buyin_feature = ["Buyin", "Members", "Nodes"]
        for feature in buyin_feature:
            buyin_store.append([feature])

        self.buyin_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)

        self.buyin_combo = Gtk.ComboBox.new_with_model(buyin_store)
        self.buyin_combo.connect("changed", self.on_buyin_combo_changed)

        renderer_text = Gtk.CellRendererText()
        self.buyin_combo.pack_start(renderer_text, True)
        self.buyin_combo.add_attribute(renderer_text, "text", 0)
        self.buyin_combo.set_active(0)
        self.buyin_combo.set_name("jobs")

        renderer_text.set_property("xalign", 0.51)

        self.buyin_box.pack_start(self.buyin_combo, False, False, True)

        #
        # View Jobs Combo Box
        #
        jobs_store = Gtk.ListStore(str)
        jobs = ["Jobs", "View Job", "Job Script"]
        for job in jobs:
            jobs_store.append([job])

        self.job_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)

        self.job_combo = Gtk.ComboBox.new_with_model(jobs_store)
        self.job_combo.connect("changed", self.on_job_combo_changed)

        renderer_text = Gtk.CellRendererText()
        self.job_combo.pack_start(renderer_text, True)
        self.job_combo.add_attribute(renderer_text, "text", 0)
        self.job_combo.set_active(0)
        self.job_combo.set_name("jobs")

        # Sets the positions of the dropdown items in the "jobs" combobox
        renderer_text.set_property("xalign", 0.51)

        self.job_box.pack_start(self.job_combo, False, False, True)

        #
        # Manage Job Combo Box
        #
        submit_store = Gtk.ListStore(str)
        submits = ["Manage Job", "Job Script", "Submit Jobs"]
        for submit in submits:
            submit_store.append([submit])

        self.submit_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)

        self.submit_combo = Gtk.ComboBox.new_with_model(submit_store)
        self.submit_combo.connect("changed", self.on_submit_combo_changed)

        renderer_text = Gtk.CellRendererText()
        self.submit_combo.pack_start(renderer_text, True)
        self.submit_combo.add_attribute(renderer_text, "text", 0)
        self.submit_combo.set_active(0)
        self.submit_combo.set_name("scripts")

        # Sets the positions of the dropdown items in the "jobs" combobox
        renderer_text.set_property("xalign", 0.51)

        self.submit_box.pack_start(self.submit_combo, False, False, True)

        #
        # Apps Combo Box
        #
        app_store = Gtk.ListStore(str)
        apps = ["Apps", "File Explorer", "Browser", "Terminal"]
        for app in apps:
            app_store.append([app])

        self.apps_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)

        self.apps_combo = Gtk.ComboBox.new_with_model(app_store)
        self.apps_combo.connect("changed", self.on_apps_combo_changed)

        renderer_text = Gtk.CellRendererText()
        self.apps_combo.pack_start(renderer_text, True)
        self.apps_combo.add_attribute(renderer_text, "text", 0)
        self.apps_combo.set_active(0)
        self.apps_combo.set_name("apps")

        # Sets the positions of the dropdown items in the "jobs" combobox
        renderer_text.set_property("xalign", 0.51)

        self.apps_box.pack_start(self.apps_combo, False, False, True)

        # self.grid.attach(function,y,x,1,1) place this grid at (y,x) and make it fill (1,1)
        self.grid.attach(self.button_home, 0, 1, 1, 1)
        self.grid.attach(self.button_status, 0, 2, 1, 1)
        self.grid.attach(self.job_box, 0, 4, 1, 1)
        self.grid.attach(self.submit_box, 0, 5, 1, 1)
        self.grid.attach(self.button_support, 0, 6, 1, 1)
        self.grid.attach(self.apps_box, 0, 7, 1, 1)
        self.grid.attach(self.button_exit, 0, 9, 1, 1)

        # if the user has buyin account, show his buyin node info
        if self.buyinAccount:
            #self.button_buyin = Gtk.Button(label=" BuyinNodes ")
            #self.button_buyin.connect("clicked", self.on_buyin_combo_changed)
            self.grid.attach(self.buyin_box, 0, 3, 1, 1)

        self.grid.attach(self.activeWindow, 1, 1, 4, 1)

        self.stored_node_info_command = "pns -N -a | sed -n '1!p' | awk '{print $1 \",\" $2 \",\" $3 \",\" $4 \",\" $5 \",\" \
         $6 \",\" $7 \",\" $8 \",\" $9 \",\" $10}'"



    def set_process_id(self,id):
        '''
        Save the process id, so we can kill shut down the program later on by using the id to kill the program
        '''
        self.process_id = id


    def add_task(self, task, type):
        '''
        Add a new process to the task list
        '''
        task.start()
        self.task_list[type].append(task)

    def set_node_queue(self, q):
        self.node_queue = q

    def set_job_queue(self, q):
        self.job_queue = q

    def on_click_manage_job(self, button):
        self.submit_popover.set_relative_to(button)
        self.submit_popover.show_all()

    def close_active_windows(self):
        self.home_window_open = False
        self.support_window_open = False

    def on_buyin_combo_changed(self, combo):
        self.leave()

        tree_iter = combo.get_active_iter()

        if tree_iter:
            model = combo.get_model()
            node = model[tree_iter][0]
            self.buyin_combo.set_active(0)
            if node == 'Nodes':
                self.buyinClick(combo)

            if node == 'Members':
                self.memberClick(combo)


    def on_job_combo_changed(self, combo):
        '''
            Control handler for the View Jobs Combo Box
        '''

        #delete!!
        self.leave()

        # Check if the combo Box is created
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            job = model[tree_iter][0]
            if job == "View Job":
                self.job_combo.set_active(0)
                self.windowSetUp(None, "job_window")


            elif job == "Job Script":
                self.job_combo.set_active(0)
                pass


    def on_submit_combo_changed(self, combo):
        '''
            Control handler for the Manage Jobs Combo Box
        '''
        self.leave()
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            selected = model[tree_iter][0]
            if selected == "Manage Job":
                self.submit_combo.set_active(0)
            elif selected == "Job Script":
                self.submit_combo.set_active(0)
                self.job_script_click()
            elif selected == "Submit Jobs":
                self.submit_combo.set_active(0)
                self.submit_job_click()

    def on_apps_combo_changed(self, combo):
        '''
            Control handler for the Apps Combo Box
        '''
        self.leave()
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            selected = model[tree_iter][0]
            if selected == "Apps":
                self.apps_combo.set_active(0)
            elif selected == "File Explorer":
                self.apps_combo.set_active(0)
                self.fileExplorerClick()
            elif selected == "Browser":
                self.apps_combo.set_active(0)
                self.browserClick()
            elif selected == "Terminal":
                self.apps_combo.set_active(0)
                self.terminalClick()


    def pendingClick(self):
        '''
        "Pending" button on jobs dropdown menu that displays a window of all pending jobs when clicked
        '''
        self.activeSort = "pending"
        self.close_active_windows()
        win = JobWindow(self.activeSort)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def runningClick(self):
        '''
        "Running" button on jobs dropdown menu that displays a window of all running jobs when clicked
        '''
        self.activeSort = "running"
        self.close_active_windows()
        win = JobWindow(self.activeSort)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def completedClick(self):
        '''
        "Completed" button on jobs dropdown menu that displays a window of all recently completely jobs when clicked
        '''
        self.activeSort = "completed"
        self.close_active_windows()
        win = JobWindow(self.activeSort)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def windowSetUp(self, button, type):
        '''
        Create the specific type of window when the button is clicked
        '''
        self.leave()
        self.close_active_windows()
        window = True
        if type == "node_window":
            window = self.node_window
            queue = self.node_queue
            if not window:
                self.button_status.set_label("Loading...")
                while Gtk.events_pending():
                    Gtk.main_iteration()

        elif type == "job_window":
            window = self.job_window
            queue = self.job_queue


        if not window:
            try:
                if self.task_list[type]:
                    self.task_list[type].pop(0).join()

                if not queue.empty():
                    if type == "node_window":
                        self.button_status.set_label("  Nodes     ")
                        self.node_window = queue.get()
                        window = self.node_window
                        time = 60

                    elif type == "job_window":
                        self.job_window = queue.get()
                        window = self.job_window

                        self.add_job_status_button(window)

                window.connect("delete-event", self.clickable, type)
                window.show_all()
                window.refresh_data(type=type, refresh_time=time)


            except Exception as e:
                print(e)

        else:
            window.connect("delete-event", self.clickable, type)
            window.show_all()

    def add_job_status_button(self, window):
        job_status = ["All", "Running", "Pending", "Complete"]
        job_store = Gtk.ListStore(str,int)
        for status in range(len(job_status)):
            job_store.append([job_status[status],status])

        job_combo = Gtk.ComboBox.new_with_model(job_store)
        renderer_text = Gtk.CellRendererText()
        job_combo.pack_start(renderer_text, True)
        job_combo.add_attribute(renderer_text, "text", 0)
        job_combo.set_active(0)
        job_combo.connect("changed", self.on_status_button_change, window)
        job_combo.set_name("jobs")
        vbox = Gtk.VBox()
        vbox.pack_start(job_combo, False, False, 0)
        window.header_bar_add_back(vbox, 15)



    def on_status_button_change(self, combo, window):
        '''
        The job window will update the info according to the job status that user selects
        '''
        status = combo.get_model()[combo.get_active_iter()][0]

        # Set status to the corresponding status value then create a new command for fetching new data
        if status == "Running":
            new_status = "R "
            print('Running!')

        elif status == "Pending":
            new_status = "PD "
            print("Pending!")

        elif status == "Complete":
            new_status = "CD "
            print("Complete!")

        else:
            status = ""
            print("All!")
            return

        command = "qs -a -t " + new_status + "| sed -n '1,2!p' | awk '{print}'"
        command += " > /tmp/GM_cur_job_info"
        subprocess.check_output(command, shell=True).decode("utf-8")

        # Convert data into a list of lists
        with open("/tmp/GM_cur_job_info") as cur_job:
            details = [line.strip().split()[:11] + [" ".join(line.strip().split()[11:])] for line in cur_job]
        window.listmodel_clear()

        # Get rid of the title
        details.pop(0)

        # Add new data to each row of window
        for d in details:
            window.listmodel_add(d)




    def memberClick(self, button):
        '''
        Display member names under each group
        '''
        self.leave()
        self.close_active_windows()
        while Gtk.events_pending():
            Gtk.main_iteration()

        buyinInfoWindow = DisplayWindow(400,300,title="Buyin members")
        groups = self.getBuyin().split()
        info = ''
        for name in groups:
            # Get all members under this group
            output = buyinInfoWindow.get_command_result("sacctmgr -Pn show assoc account=" + name + \
                                                      " | cut -d'|' -f3 |grep -Ev '^$' | tr '\n' ' ' ")
            info += '<b>' + name + '</b> (' + str(len(output)) + ' members) '+ ': \n'
            output.sort()

            # Split the members into small groups, each group contains 5 members
            output = [ output[i:i+5] for i in range(0,len(output),5) ]
            for member in output:
                new_line = ' '.join(member)
                info += '   ' + new_line + '\n'

            info += '\n\n'

        buyinInfoWindow.add_text(info)
        win = buyinInfoWindow
        win.connect("destroy", self.clickable, 'buyin')
        win.show_all()
        Gtk.main()

    def buyinClick(self,button):
        '''
        Display all buyin nodes
        '''
        if self.buyin_clickable:
            self.buyin_clickable = False
            self.leave()
            self.close_active_windows()

            while Gtk.events_pending():
                Gtk.main_iteration()

            buyinWindow = DisplayWindow(760,450,title="Buyin nodes information")

            # Decide how many columns the window needs and type of each column
            buyinWindow.set_listmodel(Gtk.ListStore(str, str, str, str, str, str,str))

            # Fetch information about buyin nodes by using this command
            command = "buyin_node_format -n " + os.environ['USER'] + " | awk '{print $1 \",\" $2 \",\" $3 \",\" $4 \",\" $5 \",\" \
                     $6 \",\" $7 }'"

            # Create the actual window when command and information form are set up
            buyinWindow.real_window(command=command, nodes_detail='buyin')

            win = buyinWindow
            win.connect("destroy", self.clickable, 'buyin')
            win.show_all()
            Gtk.main()


    def clickable(self,widget, arg2, type=None):
        '''
        hide the window when the close button gets clicked
        '''
        if type == 'buyin':
            self.buyin_clickable = True
        elif type == "node_window":
            self.node_window.hide()
        elif type == "job_window":
            self.job_window.hide()

        return True

    def update_node_window(self, servername, q):
        node_window = DisplayWindow(960, 600, title="Node Info")
        stored_node_info_command =  "./Test | awk '{print $1 \",\" $2 \",\" $3 \",\" $4 \",\" $5 \",\" \
             $6 \",\" $7 \",\" $8 \",\" substr($9,10) }' "

        node_window.real_window(command=stored_node_info_command, nodes_detail='general')
        q.put(node_window)


    def exitClick(self, button):
        '''
        Closes the application
        '''
        try:
            os.system('kill %d' % self.process_id)
        except Exception as e:
            print(e)


    def get_groups(self):
        '''
        Gets all the users groups
        '''
        out = subprocess.check_output(['groups']).decode("utf-8").rstrip()
        out = "<b>Groups: </b>" + out
        return out

    def get_quota(self):
        '''
        Gets the users home directory quota usage
        '''
        command = ["df -h /mnt/home/" + os.environ['USER'] + "/", "awk '{print $2\"  \"$3\"  \"$4}'"]

        out1 = subprocess.Popen(command[0], stdout=subprocess.PIPE, shell=True)
        out2 = subprocess.check_output(command[1], stdin=out1.stdout, shell=True)
        quotaInfo = out2.decode("utf-8").rstrip().split()

        quota = "<b>Storage Info: </b>\n     " + quotaInfo[4] + "/" + quotaInfo[5] + " Available      "

        return quota

    def getBuyin(self):
        '''
            Gets the users buyin account
            prs is equal to priority_status as it cannot use alias in bash
        '''
        #-u alexrd (account for testing buyin information
        # os.environ['USER']
        accounts = subprocess.check_output( "buyin_status -u " + os.environ['USER'] +" -l | grep Accounts | cut -d \":\" -f2", shell=True ).decode("utf-8").rstrip().split()

        account_list = ''

        if not accounts:
            return None

        for account in accounts:
            account_list += account + ' '

        return account_list

    def get_current_status(self):
        '''
            Gets the current status which is displayed when the user first logs into hpcc
        '''
        try:
            command = ["cat", "/opt/software/motd/current"]
            output = subprocess.check_output(command).decode("utf-8").rstrip()
            parse_output = output.split("\n")
            parse_output = parse_output[3:-3]
            parsed_list = []
            for line in parse_output:
                parsed_list.append(self.trimTilWhitespace(line))

            final_output = "\n".join(parsed_list)
            final_output = "<b>System Status:</b>\n" + final_output
            return final_output
        except:
            return "<b>System Status:</b>\n     Status Unavailable"

    def trimTilWhitespace(self, inputStr):
        '''
            Trims till first and last whitespace on a string, then trims that whitespace too
        '''
        first = 0
        for ndx, char in enumerate(inputStr):
            if char == " ":
                first = ndx
                break

        last = -1
        for ndx, char in reversed(list(enumerate(inputStr))):
            if char == " ":
                last = ndx
                break
        trimmed = inputStr[first:last].rstrip()
        return trimmed


    def homeClick(self, button):
        '''
        Returns the main program to the home display
        '''
        self.leave()
        if not self.home_window_open:
            # Sets active window back to our original home message and displays it
            self.activeWindow = Gtk.Label()
            self.activeWindow.set_markup(self.homeInfo + "\n <b>Version</b>:  3.0")


            self.grid.attach(self.activeWindow, 1, 1, 4, 10)
            win.connect("destroy", Gtk.main_quit)
            win.show_all()
            win.set_opacity(0)
        self.home_window_open = not self.home_window_open
        if self.support_window_open:
            self.support_window_open = False

    def leave(self):
        '''
        Clears what is in the activeWindow and sets the main window back to the original size
        '''
        if self.activeWindow.get_label() != "":
            self.grid.remove(self.activeWindow)
            self.activeWindow = Gtk.Label(label="")
            self.resize(100, 100)

    def supportClick(self, button):
        '''
        Displays support information on the main window
        '''
        self.leave()
        if not self.support_window_open:
            support_text = "<b>INSTITUTE FOR CYBER-ENBABLED RESEARCH</b> \n Biomedical &amp; Physical Sciences Building \n 567 Wilson Road, Room 1440 \n East Lansing, MI 48824 \n Monday - Friday, 9AM - 5PM \n 517-353-9509 \n https://contact.icer.msu.edu/contact for support"
            self.contact_label = Gtk.Label()
            self.contact_label.set_markup(support_text)
            self.contact_label.set_selectable(True)
            self.contact_label.set_name("contact")
            self.activeWindow = self.contact_label
            self.grid.attach(self.activeWindow, 1, 1, 4, 10)
            win.show_all()
        self.support_window_open = not self.support_window_open
        if self.home_window_open:
            self.home_window_open = False

    def job_script_click(self):
        '''
        "Job Script" button of Job Submission drop down, opens window for job script creation
        '''

        win = NewJobScript()
        win.create_advanced_version()
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def submit_job_click(self):
        '''
        "Submit Job" button of Job Submission drop down, opens window for submission
        '''
        win = SubmitJobWindow()
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def createTreeViewChart(self, columns, rows, listmodel):
        '''
        Function to build data charts
        Based off example from: https://developer.gnome.org/gnome-devel-demos/stable/treeview_simple_liststore.py.html.en
        '''
        # append the values in the model
        for r in rows:
            listmodel.append(r)


        # a treeview to see the data stored in the model
        self.view = Gtk.TreeView(model=listmodel)


        # for each column
        for i, column in enumerate(columns):
            # cellrenderer to render the text
            cell = Gtk.CellRendererText()
            cell.set_alignment(0.5, 0.5)
            # the text in the first column should be in boldface
            if i == 0:
                cell.props.weight_set = True
                cell.props.weight = Pango.Weight.BOLD

            # the column is created
            col = Gtk.TreeViewColumn(column, cell, text=i)
            col.set_alignment(0.5)
            col.set_sort_column_id(i)
            # set column GRES width to 96
            if i == 8:
                col.set_fixed_width(105)
            # and it is appended to the treeview
            self.view.append_column(col)

        # Makes the chart scrollable
        self.scroll = Gtk.ScrolledWindow(hexpand=True, vexpand=True)

        self.scroll.add(self.view)

        info_window = Gtk.VBox()
        info_window.pack_start(self.scroll, True, True, 5)

        return info_window




    def fileExplorerClick(self):
        '''
        Button that opens a file explorer
        '''


        # Hold for now
        self.leave()
        self.close_active_windows()

        p = subprocess.Popen(["nautilus"])
        self.processList.append(p)

        win.show_all()

    def browserClick(self):
        '''
        Button that opens the Firefox browser
        '''

        self.leave()
        self.close_active_windows()

        p = subprocess.Popen(["firefox"])
        if p.poll() is not None:
            p.kill()
        self.processList.append(p)

        win.show_all()

    def terminalClick(self):
        '''
        Button that opens an instance of the "xterm" terminal
        '''
        self.leave()
        self.close_active_windows()

        p = subprocess.Popen(["xterm"])
        self.processList.append(p)

        win.show_all()





class JobWindow(MainWindow):
    '''
    Class that creates main window for jobs
    '''

    def __init__(self, activeSort):
        Gtk.Window.__init__(self, title="Jobs Info")
        self.activeSort = activeSort
        self.window_is_open = True
        self.set_border_width(10)
        self.set_default_size(960, 600)
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "Jobs Info"
        self.label = Gtk.Label()
        header = Gtk.Label(label="Number of Active Jobs: ")
        header.set_justify(Gtk.Justification.CENTER)

        # Hardcode of the columns we will print for job
        columns = ["JOBID", "USER", "PARTITION", "NAME", "NODE", "CPUS", "MIN_ME", "START_TIME", "TIME_LIMIT",
                   "TIME_ELAPS", "ST", "NODELIST(REASON)"]

        # get job information
        out = self.get_jobs(self.activeSort)

        if out != None:

            rows = []
            for i in range(0, len(out), 12):
                rows.append(out[i: i + 12])

            self.listmodel = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str)

            super().createTreeViewChart(columns, rows, self.listmodel)

            #self.view.connect("row-activated", self.present_job_details, self.listmodel)
            Thread(target=self.refresh_model, args=()).start()

        else:
            self.label = Gtk.Label()
            self.label.set_text("No jobs with this state")
            self.add(self.label)
        # Kill the application once the user closes the window?
        self.connect("destroy", self.destroy)

    def destroy(self, widget, data=None):
        self.window_is_open = False
        Gtk.main_quit()

    def get_jobs(self, filter):
        '''
        Applies filters to jobs and displays them correspondingly
        '''
        if filter == "all":
            out = subprocess.check_output(["qs"]).decode("utf-8").rstrip()
            out = out.split()[18:]
        elif filter == "pending":
            try:
                out = subprocess.check_output("qs | grep ' PD '", shell=True).decode("utf-8").rstrip()
                out = out.split()
            except subprocess.CalledProcessError:
                out = None
        elif filter == "running":
            try:
                out = subprocess.check_output("qs | grep ' R '", shell=True).decode("utf-8").rstrip()
                out = out.split()
            except subprocess.CalledProcessError:
                out = None
        elif filter == "completed":
            try:
                out = subprocess.check_output("qs | grep ' CG '", shell=True).decode("utf-8").rstrip()
                out = out.split()
            except subprocess.CalledProcessError:
                out = None
        return out

    def refresh_model(self):
        while True:
            start = time.time()
            while time.time() - start < 30:
                if not self.window_is_open:
                    return
            GLib.idle_add(self.refresh)

    def refresh(self):
        # clear previous model
        jobs = self.get_jobs(self.activeSort)
        rows = []
        if len(jobs) % 12 != 0:
            # prevent bad info from being entered
            return
        for i in range(0, len(jobs), 12):
            rows.append(jobs[i: i + 12])
        self.listmodel.clear()
        # append the values in the model
        for i in range(len(rows)):
            if len(rows[i]) == 12:
                self.listmodel.append(rows[i])
        self.view.columns_autosize()

    def show_job(self, id):
        '''
        Gets show job information from a job id
        Returns a list of lists of info from "scontrol -o show job <jobid>"
        '''
        command = "scontrol -o show job " + str(id)
        args = shlex.split(command)
        output = subprocess.check_output(args).decode("utf-8").rstrip()

        job_info = list()
        for info in output.split():
            temp = info.split("=")
            if len(temp) == 2:
                job_info.append(temp)

        return job_info

    def present_job_details(self, treeview, path, view_column, liststore):
        '''
        Creates a window which presents more details about a job
        '''
        treeiter = liststore.get_iter(path)
        jobid = liststore.get_value(treeiter, 0)
        details = self.show_job(jobid)

        win = DetailsWindow(details)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

class DisplayWindow(MainWindow):
    '''
    Class for displaying additional information about nodes and jobs
    '''


    def __init__(self, width=500, height=500, border=10,title='general window'):
        Gtk.Window.__init__(self, title=title)
        self.width = width     # default window width
        self.height = height    # default window height
        self.border_width = border      # default border width
        self.listmodel = Gtk.ListStore(str)
        self.content = Gtk.HBox()
        self.queue = queue.Queue()
        self.stop_threads = False
        self.task_list = []
        self.list_container = Gtk.VBox()
        self.view = None
        # default border width
        self.set_border_width(self.border_width)

        # set window to default size
        self.set_default_size(self.width, self.height)
        self.set_position(Gtk.WindowPosition.CENTER)


        self.vbox = Gtk.VBox()
        self.header_bar = Gtk.HBox()

        # put the search entry in a grid before add it to the header_bar
        self.search = Gtk.SearchEntry()
        self.search.connect("search-changed", self._on_search)
        grid = Gtk.Grid()
        grid.attach(self.search, 1, 1, 2, 1)
        self.header_bar.pack_start(grid, False, False, 0)

        # put header_bar into the display window
        self.vbox.pack_start(self.header_bar, False, False, 0)

        self.refresh_button = Gtk.Button()
        self.refresh_button.set_name("header_button")
        self.refresh_button.set_label("Refresh")
        self.refresh_button.connect("clicked", self.refresh_clicked)
        self.header_bar.pack_end(self.refresh_button, False, False, 15)

        self.filter_button = Gtk.Button()
        self.filter_button.connect("clicked", self.filter_clicked)
        self.filter_button.set_name("header_button")
        self.filter_button.set_label("Filter")
        self.header_bar.pack_end(self.filter_button, False, False, 0)

        self.hbox = Gtk.HBox()
        self.keywords = None

    def header_bar_add_back(self, button, d):
        '''
        button: The button that we want to add to the back of the header_bar
        d: The distance between the button and last button
        This function would push back a new button to the header_bar
        '''
        self.header_bar.pack_end(button, False, False, d)


    def filter_clicked(self, widget):
        columns = [ [c.get_title(), c] for c in self.view.get_columns()]
        win = Gtk.Window()
        win.connect("destroy", Gtk.main_quit)

        bound = 3
        v_box = Gtk.VBox()
        while columns:
            h_box = Gtk.HBox()
            for _ in range(bound):
                if not columns:
                    break
                cur_col = columns.pop(0)
                button = Gtk.CheckButton()
                button.set_label(cur_col[0])
                button.set_active(cur_col[1].get_visible())
                button.connect("clicked", self.column_hide, cur_col[1])
                h_box.pack_start(button, False, False, 10)
            v_box.pack_start(h_box, False, False, 0)
        win.add(v_box)
        win.show_all()
        Gtk.main()


    def column_hide(self, widget, column):
        column.set_visible(not column.get_visible())

    def listmodel_clear(self):
        '''
        Remove all items from listmodel
        '''
        self.listmodel.clear()


    def listmodel_add(self, item):
        self.listmodel.append(item)


    def refresh_data(self, type=None, command=None, refresh_time=None):
        '''
        This fucntion will refresh data windown every 20 seconds
        method: create a new status window
        '''
        if self.task_list:
            self.task_list.clear()

        if type == "node_window":
            command =  "./Test | awk '{print $1 \",\" $2 \",\" $3 \",\" $4 \",\" $5 \",\" \
             $6 \",\" $7 \",\" $8 \",\" substr($9,10) }' "
            self.add_task(["Timer", threading.Timer(refresh_time, self.refresh_data, args=(type, command, refresh_time))])
            self.add_task(["Process", Thread(target=self.new_data, args=("server01", self.queue, command))])

    def add_task(self, task):
        '''Start the task and then add it to the task list'''
        task[1].start()
        self.task_list.append(task)


    def new_data(self, server_name, q, command=None):
        '''Fetch the new data'''
        print("Collecting new data!")
        details = []
        output = self.get_command_result(command)

        # cut each line of output into a list
        for d in range(len(output)):
            row = output[d].split(',')
            if not row[1]:
                new_row = output[d - 1].split(',')
                new_row[1] = row[0]
                row = new_row
            details.append(row)

        # remove the old data before put new data
        if not q.empty():
            with q.mutex:
                q.queue.clear()

        q.put(details)


    def refresh_clicked(self, widget):
        '''When the fresh button gets clicked, the window content will be changed when the new information is fetched'''
        if not self.queue.empty():
            data = self.queue.get()
            data.pop(0)

            # Delete old info
            self.listmodel.clear()

            # Add new new info for each row
            for r in data:
                self.listmodel.append(r)
            self.filter.refilter()
            self.view.set_model(Gtk.TreeModelSort(self.filter))
            print("ready to refresh!")

        else:
            print("not ready!")



    def list_filter_func(self, lista, user_data):
        key_word = self.search.get_text()
        if not key_word:
            return lista

        rows_info = lista.get_child().get_children()
        for rows_name in rows_info:
            if key_word.lower() in rows_name.get_text().lower():
                return lista

            elif "NODELIST" in rows_name.get_text():
                return lista

    def set_refresh_func(self, func):
        self.refresh_button.connect('clicked', func)


    def add_text(self, content=''):
        new_text = Gtk.Label()
        new_text.set_markup(content)
        self.add(new_text)

    def set_window_height(self,height):
        ''' Reset window height'''
        self.height = height

    def set_window_width(self,width):
        ''' Reset window width'''
        self.width = width

    def set_listmodel(self,listmodel):
        '''
        Reset listmodel
        Listmodel is a listStore which contains all contents
        '''
        self.listmodel = listmodel

    def get_command_result(self, command):
        '''
        command is a string input
        return a list which contains the result of the command
        '''

        output = subprocess.check_output(command, shell = True).decode("utf-8").rstrip().split()

        return output

    def real_window(self, header=None, command='', row_content=None, nodes_detail=""):
        ''' Put the output of the command into the actual window'''
        details = []

        try:
            if command:
                if nodes_detail == "general":
                    output = self.get_command_result(command)
                    nodes_listed = {}
                    index = 0
                    # cut each line of output into a list
                    for d in range(len(output)):
                        row = output[d].split(',')[:-1]
                        if not row[1]:
                            new_row = details[-1][:]
                            new_row[1] = row[0]
                            row = new_row
                        # Check if the node name is already in our dict
                        if row[0] in nodes_listed:
                            # Add new partition to that node
                            details[nodes_listed[row[0]]][1] += ", " + row[1]
                        else:
                            # Otherwise we have a new node name
                            nodes_listed[row[0]] = index
                            index += 1
                            details.append(row)

                elif nodes_detail == "job_info":
                    command += " > /tmp/GM_cur_job_info"
                    subprocess.check_output(command, shell=True).decode("utf-8")
                    with open("/tmp/GM_cur_job_info") as cur_job:
                        details = [line.strip().split()[:11] + [" ".join(line.strip().split()[11:])] for line in cur_job]


            # if the input content can be used without any change then just use it
            elif row_content:
                details = row_content

            # if the header is not set, then take the first line as header
            if not header:
                header = details.pop(0)

            if nodes_detail == "general":

                self.scroll_vbox = Gtk.VBox()

                win = super().createTreeViewChart(header, details, self.listmodel)

            else:
                win = super().createTreeViewChart(header, details, self.listmodel)

            self.content.pack_start(win, True, True, 0)
            self.vbox.pack_start(self.content, True, True, 0)
            self.view.connect("row-activated", self.present_node_details, self.listmodel)
            self.filter = self.listmodel.filter_new()
            self.filter.set_visible_func(self.on_select_row)
            self.add(self.vbox)


        except Exception as e:
            print("error: ")
            print(e)


    def on_select_row(self, model, iter, data):
        '''This funcgtion defines the method for searching'''

        if not self.keywords:
            return True

        for i in model[iter]:
            if self.keywords.lower() in i.lower():
                return True

        return False

    def _on_search(self, entry):
        '''
        Filter the displayed data according to the content in the search button
        '''
        self.keywords = entry.get_text()
        self.filter.refilter()
        self.view.set_model(Gtk.TreeModelSort(self.filter))
        self.show_all()

    def present_buyin_node_details(self,treeview, path, view_column, liststore):
        treeiter = liststore.get_iter(path)
        node = liststore.get_value(treeiter,0)
        command = "buyin_node_format -j " + str(node) + " | awk '{print $1 \",\" $2 \",\" $3 \",\" $4l \",\" $5 \",\" $6}'"

        win = DisplayWindow(title='Node jobs')
        win.set_listmodel(Gtk.ListStore(str, str, str, str, str, str))
        win.real_window(command=command)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def present_node_details(self,treeview, path, view_column, liststore):
        ''' Creates a window which presents more details about a job'''

        treeiter = liststore.get_iter(path)
        node = liststore.get_value(treeiter,0)
        details = self.show_node(node)

        win = DisplayWindow(title="Job Details")
        win.set_listmodel(Gtk.ListStore(str, str))
        win.real_window(header=["Category", "Value"], row_content=details)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def present_job_details(self, treeview, path, view_column, liststore):
        treeiter = liststore.get_iter(path)
        job = liststore.get_value(treeiter, 0)
        details = self.show_job_det(job)
        
        win = DisplayWindow(title="Job Details")
        win.set_listmodel(Gtk.ListStore(str, str))
        win.real_window(header=["Category", "Value"], row_content=details)
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def show_job_det(self, job):
        command = "scontrol -o show job " + str(job)
        args = shlex.split(command)
        output = subprocess.check_output(args).decode("utf-8").rstrip()

        job_info = list()
        for info in output.split():
            temp = info.split("=")
            if len(temp) == 2:
                job_info.append(temp)

        return job_info

    def show_node(self,node):
        # Gets show job information from a node
        # Returns a list of lists of info from "scontrol -o show node <jobid>"

        command = "scontrol -o show node " + str(node)
        args = shlex.split(command)
        output = subprocess.check_output(args).decode("utf-8").rstrip()

        node_info = list()
        for info in output.split():
            temp = info.split("=")
            if len(temp) == 2:
                node_info.append(temp)

        return node_info


    def add_to_windown(self, content):
        '''
        This is an empty base function which can be override later on
        '''
        self.add(content)

class NewJobScript(DisplayWindow):
    def __init__(self):
        # Window and grid creation
        DisplayWindow.__init__(self, width=500,height=700, title="Job Submission Script")
        self.set_size_request(500, 700)
        self.set_resizable(False)

        # stores all boxes that show up in window
        self.box_list = Gtk.VBox()
        self.box_list.set_border_width(0)

        #Create container for scroll and tabs
        self.jobvbox = Gtk.VBox()

        #create array to store the advanced entry boxes in
        self.adv_entries = []

        # path of file that is chosen to be loaded
        self.loaded_file_loc = ''

        # initial_time = [hours, mins, secs]
        # initialize to 1 min
        self.initial_time = [0, 1]

        # initial_vales = [nodes, tasks, cpu-per-task, mem-per-cpu]
        self.initial_values = [1, 1, 0, 0]

        # spin_buttons store all spinbuttons
        self.spin_buttons = []

        # the path of chosen file
        self.loaded_file_loc = ''

        # the path for saving files
        self.file_loc = None

        # check if the chosen file is already a sb file
        self.file_exist = False

        self.current_directory = Gtk.Label()

        self.file_path = Gtk.Label()

        self.loaded_file_folder = ''

        self.text_buffer = None
        self.check_titles = None
        self.adv_option_lookup = None
        self.file_name = ''
        self.option_combo = None
        self.preview_name = Gtk.Entry()
        self.old_script = ''

    def update_data(self):
        print('update_data')
        if self.loaded_file_loc:
            # Get time
            updated_time = self.get_command_result(
                "cat {} | grep SBATCH | grep time | cut -d '=' -f2".format(self.loaded_file_loc))
            if updated_time:
                self.initial_time = updated_time[0].split(":")

            # Get nodes
            updated_nodes = self.get_command_result(
                "cat {} | grep SBATCH | grep nodes | cut -d '=' -f2".format(self.loaded_file_loc))
            if updated_nodes:
                self.initial_values[0] = int(updated_nodes[0])

            # Get tasks
            updated_tasks = self.get_command_result(
                "cat {} | grep SBATCH | grep ntasks-per-node | cut -d '=' -f2".format(self.loaded_file_loc))
            if updated_tasks:
                self.initial_values[1] = int(updated_tasks[0])

            # Get cpu-per-task
            updated_cpt = self.get_command_result(
                "cat {} | grep SBATCH | grep cpus-per-task | cut -d '=' -f2".format(self.loaded_file_loc))
            if updated_cpt:
                self.initial_values[2] = int(updated_cpt[0])

            # Get mem-per-cpu
            updated_mpc = self.get_command_result(
                "cat {} | grep SBATCH | grep mem | cut -d '=' -f2 | grep -o -E '[0-9]+'".format(self.loaded_file_loc))
            if updated_mpc:
                self.initial_values[3] = int(updated_mpc[0])

            command = "grep -v '#' " + self.loaded_file_loc
            self.old_script = subprocess.check_output(command, shell=True).decode("utf-8").rstrip()
            self.text_buffer.set_text(text=self.old_script)

    def create_adj(self, adj_type="incr"):
        # Update hour
        if adj_type == "hour":
            return Gtk.Adjustment(value=int(self.initial_time[0]), lower=0, upper=72, step_increment=1, page_increment=1, page_size=0)

        # Update min
        elif adj_type == "min":
            return Gtk.Adjustment(value=int(self.initial_time[1]), lower=0, upper=59, step_increment=1, page_increment=1, page_size=0)

        elif adj_type == "nodes":
            return Gtk.Adjustment(value=int(self.initial_values[0]), lower=1, upper=1000, step_increment=1, page_increment=1, page_size=0)

        elif adj_type == "tasks":
            return Gtk.Adjustment(value=int(self.initial_values[1]), lower=1, upper=1000, step_increment=1, page_increment=1, page_size=0)

        elif adj_type == "cpt":
            return Gtk.Adjustment(value=int(self.initial_values[2]), lower=0, upper=1000, step_increment=1, page_increment=1, page_size=0)

        elif adj_type == "mpc":
            return Gtk.Adjustment(value=int(self.initial_values[3]), lower=0, upper=1000, step_increment=1, page_increment=1, page_size=0)

    def create_button(self, title='', type=None, start=0,end=0):
        '''
        create a button
        '''
        title_box = Gtk.Label(label=title)
        value = Gtk.SpinButton(margin_start=start,margin_end=end)
        self.spin_buttons.append([value,type])
        if type:
            value.configure(self.create_adj(adj_type=type), 0, 0)

        return [title_box, value]

    def create_advanced_version(self):
        '''
        create the main body of jobScriptWindow
        :return:
        '''

        # Remove the last script
        if not subprocess.check_output('/opt/software/GUI/GreenMambaGUI/check_folder /tmp/GreenMambaTest', shell=True).decode(
                "utf-8").strip() == 'SUCCESS':
            subprocess.Popen(['mkdir', '/tmp/GreenMambaTest'])

        else:
            if subprocess.check_output('find /tmp/GreenMambaTest/ -name ' + 'testFile-' + os.environ['USER'], shell=True).decode('utf-8'):
                command = "rm " + '/tmp/GreenMambaTest/testFile-' + os.environ['USER']
                subprocess.check_output(command, shell=True).decode("utf-8")

        box = Gtk.VBox()

        default_options_box = Gtk.VBox()

        eventbox = Gtk.EventBox()
        eventbox.connect("button-press-event", self.on_button_press_event)
        box.pack_start(eventbox, True, True, 0)

        self.box_list.pack_start(box, False, False, 0)

        # title
        file_duration = Gtk.Label()
        file_duration.set_markup("<b>Job Wall Time: </b>")
        default_options_box.pack_start(file_duration, False, False, padding=0)

        # duration box stores inputs of time
        duration = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        hours_box = Gtk.VBox()
        hour = self.create_button('Hours:', 'hour', start=0)
        hours_box.pack_start(hour[0], False, False, padding=2)
        hours_box.pack_start(hour[1], False, False, padding=5)
        duration.pack_start(hours_box, True, False, padding=2)

        mins_box = Gtk.VBox()
        min = self.create_button('Minutes:', 'min', start=0)
        mins_box.pack_start(min[0], False, False, padding=2)
        mins_box.pack_start(min[1], False, False, padding=5)
        duration.pack_start(mins_box, True, False, padding=2)

        # add duration to the box_list
        default_options_box.pack_start(duration, False, False, padding=0)

        #create Hbox's for default options
        defaultBoxOne = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        defaultBoxTwo = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        node_values = Gtk.VBox()
        node = self.create_button("Number of Nodes:", 'nodes', start=0)
        node_values.pack_start(node[0], True, False, padding=2)
        node_values.pack_start(node[1], False, False, padding=5)
        defaultBoxOne.pack_start(node_values, True, False, padding=2)

        task_values = Gtk.VBox()
        task = self.create_button("Number of Tasks:", 'tasks', start=0)
        task_values.pack_start(task[0], True, False, padding=2)
        task_values.pack_start(task[1], False, False, padding=5)
        defaultBoxOne.pack_start(task_values, True, False, padding=2)

        cpt_values = Gtk.VBox()
        cpt = self.create_button("CPUs per Task:", 'cpt', start=0)
        cpt_values.pack_start(cpt[0], False, False, padding=2)
        cpt_values.pack_start(cpt[1], False, False, padding=5)
        defaultBoxTwo.pack_start(cpt_values, True, False, padding=2)

        mpc_values = Gtk.VBox()
        mpc = self.create_button("Memory per CPU:", 'mpc', start=0)
        mpc_values.pack_start(mpc[0], False, False, padding=2)
        mpc_values.pack_start(mpc[1], False, False, padding=5)
        defaultBoxTwo.pack_start(mpc_values, True, False, padding=2)

        default_options_box.pack_start(defaultBoxOne, False, False, padding=0)
        default_options_box.pack_start(defaultBoxTwo, False, False, padding=0)

        self.box_list.pack_start(default_options_box, True, False, padding=0)

        path = Gtk.HBox()
        file_path = Gtk.Label()
        file_path.set_markup("<b>File Path: </b>")
        self.file_path.set_text('No file is seleted')
        path.pack_start(file_path, False, False, padding=5)
        path.pack_start(self.file_path, False, False, padding=5)

        advanced_lbl = Gtk.Label()
        advanced_lbl.set_markup("<b>Advanced Options: </b>")
        self.box_list.pack_start(advanced_lbl, True, False, padding=10)

        #advanced options scroll menu
        self.advanced_scroll = Gtk.ScrolledWindow()
        self.advanced_box = Gtk.VBox()
        self.advanced_scroll.add(self.advanced_box)
        self.box_list.pack_start(self.advanced_scroll, True, False, padding=0)
        self.advanced_scroll.set_size_request(350,350)
        self.advanced_scroll.set_vexpand(True)

        # multiline textbox to enter in job execution code
        text_scroll = Gtk.ScrolledWindow(margin_start=10, margin_end=5)
        text_view = Gtk.TextView()
        text_scroll.set_size_request(375, 200)
        text_scroll.add(text_view)
        text_scroll.set_vexpand(True)
        self.text_buffer = text_view.get_buffer()
        self.text_buffer.set_text(self.create_header())

        for b in self.spin_buttons:
            b[0].connect('value-changed', self.update_text)

        # checkbutton titles for advanced options
        self.check_titles = [("--array     ", "Submits a job array with n identical jobs"),
                             ("--account   ", "This option tells SLURM to use the specified buy-in Account."),
                             ("--begin     ", "Submit the batch script to the Slurm controller immediately,\n like normal, "
                                         "but tells the controller to defer the allocation of the\n job until the"
                                         " specified time."),
                             ("--constraint", "Request node feature. May be specified with symbol '&'\n for and, '|' "
                                              " for or, etc."),
                             ("--dependency", "Require ncpus number of processors per task"),
                             ("--error     ", "Defer the start of this job until the specified dependencies\n have been"
                                         " satisfied completed."),
                             ("--export    ", "Identify which environment variables are propagated to the\n launched"
                                          " application, by default all are propagated."),
                             ("--gres      ", "Specifies a comma delimited list of generic consumable resources."),
                             ("--hold      ", "Specify the job is to be submitted in a held state \n(priority of zero)."),
                             ("--immediate ", "The batch script will only be submitted to the controller\n if the"
                                             " resources necessary to grant its job allocation are\n immediately"
                                             " available."),
                             ("--input     ", "Instruct Slurm to connect the batch script's standard input\n directly to the"
                                         " file name specified in the 'filename pattern'."),
                             ("--jobname   ", "Specify a name for the job allocation."),
                             ("--licenses  ", "Specification of licenses (or other resources available\n on all nodes of"
                                            " the cluster) which must be allocated to this job."),
                             ("--mail-type ", "Notify user by email when certain event types occur."),
                             ("--mail-user ", "User to receive email notification of state changes as\n defined by"
                                             " --mail-type. "),
                             ("--mem       ", "Specify the real memory required per node."),
                             ("--output    ",
                              "Instruct Slurm to connect the batch script's standard output\n directly to the"
                              " file name specified in the 'filename pattern'."),
                             ("--tmp       ", "Specify a minimum amount of temporary disk space per node."),
                             ("--verbose   ", "Increase the verbosity of sbatch's informational messages."),
                             ("--nodelist  ", "Request a specific list of your buy-in nodes. "),
                             ("--exclude   ", "Explicitly exclude certain nodes from the resources granted\n to the job.")]

        # lookup used for fetching info from these options
        self.adv_option_lookup = dict()

        self.jobshbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        self.jobshbox.pack_start(self.box_list, False, False, 0)
        right_side = Gtk.VBox()
        right_side.pack_start(path, False, False, padding=4)
        right_side.pack_start(text_scroll, True, True, padding=5)
        self.jobshbox.pack_start(right_side, True, True, padding=0)

        #create a Notebook to be placed at the top of the window,
        self.jb_header = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=8)
        self.jb_header.set_name("Jobs_Header")
        self.jb_header.set_border_width(10)

        btnLoadFile = Gtk.Button(label="Load File")
        btnSave = Gtk.Button(label="Save")
        btnSaveAs = Gtk.Button(label="Save As")
        btnSubmit = Gtk.Button(label="Submit")

        self.jb_header.pack_start(btnLoadFile, False, False, padding=8)
        self.jb_header.pack_start(btnSave, False, False, padding=8)
        self.jb_header.pack_start(btnSaveAs, False, False, padding=8)
        self.jb_header.pack_start(btnSubmit, False, False, padding=8)

        #create actions for button clicks
        btnLoadFile.connect("clicked", self.edit_file, 'Open')
        btnSave.connect("clicked", lambda s: self.on_select(None, [None, None, 'Save']) if self.loaded_file_folder \
                else self.edit_file(None,'Save as'))
        btnSaveAs.connect("clicked", self.edit_file, 'Save as')
        btnSubmit.connect("clicked", self.submit_clicked)

        # create advanced options
        self.create_adv_options()

        self.jobvbox.pack_start(self.jb_header, False, False, 0)
        self.jobvbox.pack_start(self.jobshbox, True, True, padding=5)

        #add all containers to the box
        self.add(self.jobvbox)

    def submit_clicked(self, widget):
        if self.loaded_file_folder:
            self.on_select(None, [None, None, 'Save'])
        else:
            self.edit_file(None, 'Save as')
        try:
            output = subprocess.check_output('sbatch -D %s %s' % (self.loaded_file_folder, self.loaded_file_loc), shell = True).decode("utf-8")
            self.error_msg_window(title='Submit', msg=output)

        except Exception as e:
            print(e)

    def on_button_press_event(self, widget, event):
        # Check if right mouse button was preseed
        if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 3:
            self.popup.popup(None, None, None, None, event.button, event.time)
            return True  # event has been handled

    def create_adv_options(self):
        self.adv_initialized = False

        for title in self.check_titles:
            check = Gtk.CheckButton()
            entry = Gtk.Entry()
            button = Gtk.Button(label=title[0])
            button.set_property("width-request", 120)

            button.set_name("help")
            button.set_relief(2)

            hbox_adv_opt = Gtk.HBox()
            popover = Gtk.Popover()

            popover.set_position(Gtk.PositionType.RIGHT)
            vbox_help = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            vbox_help.pack_start(Gtk.Label(label=title[1]), False, True, 10)
            popover.add(vbox_help)

            check.connect("toggled", self.checked, entry)
            entry.connect("activate", self.update_text)
            button.connect("clicked", self.help, popover)

            hbox_adv_opt.pack_start(check, False, False, padding=1)
            hbox_adv_opt.pack_start(button, False, False, padding=1)
            hbox_adv_opt.pack_start(entry, False, False, padding=1)

            self.adv_entries.append(entry)

            self.advanced_box.pack_start(hbox_adv_opt, False, False, padding=10)

            self.adv_option_lookup[title[0]] = (check, entry)
        self.adv_initialized = True

    def help(self, button, popover):
        popover.set_relative_to(button)
        popover.show_all()

    def checked(self, widget, entry=None):
        self.update_text()

    def edit_file(self, widget, type=None):
        main_dialog = DisplayWindow(title='test', width=630)
        main_dialog.set_position(Gtk.WindowPosition.CENTER)

        box = Gtk.VBox()
        main_dialog.add(box)

        # header
        header = Gtk.HeaderBar()
        header.set_title('GreenMamba File Chooser')
        header.set_subtitle('Select Files and Folders')

        main_dialog.set_titlebar(header)

        # add a chooser
        chooser = Gtk.FileChooserWidget(Gtk.FileChooserAction.OPEN)
        chooser.set_select_multiple(True)
        chooser.set_current_folder('/mnt/home/' + os.environ['USER'])
        box.pack_start(chooser, True, True, 0)

        footer = Gtk.VBox()
        box.pack_start(footer, False, False, 0)

        if type == 'Save as':
            # display file name
            name_box = Gtk.HBox()
            if not self.preview_name.get_text():
                self.preview_name.set_text('script')

            name_box.pack_start(Gtk.Label('File Name: '), False, False, 0)
            name_box.pack_start(self.preview_name, False, False, 0)
            footer.pack_start(name_box, False, False, 0)

        # Cancel button
        cancel = Gtk.Button('Cancel')
        cancel.connect('clicked', lambda exit: main_dialog.destroy())

        # Select button
        select = Gtk.Button('Select')
        select.connect('clicked', self.on_select, [chooser, main_dialog, type])

        buttons = Gtk.HBox()
        buttons.pack_end(select, False, False, 5)
        buttons.pack_end(cancel, False, False, 5)
        footer.pack_start(buttons, False, False, 0)

        main_dialog.show_all()

    def on_select(self, widget, lst):
        '''
            Update path of file or folder when select button is clicked
        '''
        try:
            if lst[2] == 'Open':
                self.file_loc = lst[0].get_uri()[7:]
                self.loaded_file_loc = lst[0].get_uri()[7:]
                self.file_path.set_text(self.loaded_file_loc)
                self.loaded_file_folder = lst[0].get_current_folder()
                self.update_data()
                for button in self.spin_buttons:
                    button[0].configure(self.create_adj(adj_type=button[1]), 0, 0)
                self.file_name = lst[0].get_uri()[7:].split('/')[-1].replace('.sb', '')
                self.preview_name.set_text(self.file_name)
                self.update_text()
                lst[1].destroy()

            elif lst[2] == 'Save':
                with open(self.loaded_file_folder + '/' + self.file_name + '.sb', 'w') as file:
                    file.write(self.text_buffer.get_text(self.text_buffer.get_start_iter(),
                                                         self.text_buffer.get_end_iter(), False))
                    file.close()

            elif lst[2] == 'Save as':
                self.save_file(lst[0].get_uri()[7:])
                self.file_path.set_text(self.loaded_file_loc)
                lst[1].destroy()


        except Exception as e:
            if lst[2] == 'Open':
                self.error_msg_window('Please select a file!')

            if lst[2] == 'Save as':
                self.error_msg_window('Please select a folder!')
                self.loaded_file_folder = ''

            # details about the error
            print(e)

    def save_file(self, path):
        self.file_name = self.preview_name.get_text()
        if self.file_name + '.sb' in path:
            path = path.replace('/' + self.file_name + '.sb', '')

        if not self.loaded_file_folder:
            self.loaded_file_folder = path
            self.loaded_file_loc = path + '/' + self.file_name + '.sb'

        if self.get_command_result('find ' + path + ' -name ' + self.file_name + '.sb'):
            msg = MsgDialog(self, title='File window')
            msg.set_content(self.file_name + '.sb is already existed, do you want to overwrite it?')
            response = msg.run()
            if response == Gtk.ResponseType.OK:
                with open(path + '/' + self.file_name + '.sb', 'w') as file:
                    file.write(self.text_buffer.get_text(self.text_buffer.get_start_iter(),
                                                         self.text_buffer.get_end_iter(), False))
                    file.close()
                msg.destroy()
                self.error_msg_window(title='Save File', msg='Save successfully!', w=200)

            elif response == Gtk.ResponseType.CANCEL:
                msg.destroy()

        else:
            with open(path + '/' + self.file_name + '.sb', 'w') as file:
                file.write(self.text_buffer.get_text(self.text_buffer.get_start_iter(),
                                                     self.text_buffer.get_end_iter(), False))
                file.close()
                self.error_msg_window(title='Save File', msg='Save successfully!', w=200)

    def update_text(self, widget=None):
        with open('/tmp/GreenMambaTest/testFile-' + os.environ['USER'], "w") as file:
            try:
                file.write(self.text_buffer.get_text(self.text_buffer.get_start_iter(), self.text_buffer.get_end_iter(), False))
                file.close()
                command = "grep -v '#' " + '/tmp/GreenMambaTest/testFile-' + os.environ['USER']
                self.old_script = subprocess.check_output(command, shell=True).decode("utf-8")
                if self.old_script:
                    self.old_script = self.old_script.strip()
            except Exception as e:
                print(e)

        header = self.create_header()
        self.text_buffer.set_text(header + self.old_script)

    def create_header(self):
        # The real script
        SH_BANG = "#!/bin/bash --login"
        SBATCH = "#SBATCH"

        adv_headers = []

        time = "%s --time=%s \n" % (SBATCH, self.get_time_formatted())
        nodes = "%s --nodes=%s  \n" % \
                (SBATCH, self.spin_buttons[2][0].get_text()) if self.spin_buttons[2][0].get_value() else ""

        ntasks = "%s --ntasks=%s \n" % \
                 (SBATCH, self.spin_buttons[3][0].get_text()) if self.spin_buttons[3][0].get_value() else ""

        cpus_per_task = "%s --cpus-per-task=%s \n" % \
                        (SBATCH, self.spin_buttons[4][0].get_text()) if self.spin_buttons[4][0].get_value() else ""

        mem_per_cpu = "%s --mem-per-cpu=%sG \n" % \
                      (SBATCH, self.spin_buttons[5][0].get_text()) if self.spin_buttons[5][0].get_value() else ""

        #format and create text for advanced option headers.
        iterator = 0
        if(self.check_titles and self.adv_initialized):
            for title in self.check_titles:
                title_str = "%s %s=%s \n" % \
                            (SBATCH, title[0].replace(" ", ""), self.adv_entries[iterator].get_text()) if self.adv_entries[iterator].get_text() and self.adv_option_lookup[title[0]][0].get_active() else ""
                adv_headers.append(title_str)

                iterator += 1

        '''header = SH_BANG + "\n" + time + "\n" \
                  + nodes + "\n" + ntasks + "\n" \
                  + cpus_per_task + "\n" + mem_per_cpu + "\n"'''
        header = SH_BANG + "\n" + time + nodes + ntasks + cpus_per_task + mem_per_cpu

        #updating the advanced options
        if(adv_headers):
            for adv_header in adv_headers:
                header += adv_header

        if self.loaded_file_folder:
            folder = self.loaded_file_folder.split('/')[-1]
            chdir = "%s --chdir=%s" % (SBATCH, folder)
            header += chdir + "\n"

        return header+"\n"

    def error_msg_window(self, msg='', w=100, h=100, title='Error'):
        '''
            pop out an error message on a new window
            :param msg: the content display on window
        '''
        error_msg = DisplayWindow(title=title, width=w, height=h)
        error_msg.add_text(msg)
        error_msg.show_all()

    def create_script(self, save_path, version):
        '''
            Take a script and create a .sb file with that job script
        '''
        script = self.build_job_sub_script(save_path, version)
        if script:
            path = save_path + "/" + self.file_name + ".sb"
            with open(path, "w") as file:
                try:
                    file.write(script)
                    file.close()
                    # self.file_write_success.set_text("Success!")
                    self.error_msg_window("Success!", w=130)
                except Exception as er:
                    print(er)
                    self.file_write_success.set_text("Error writing to file!")
            #self.file_write_success.set_visible(True)

    def get_time_formatted(self):
        '''
            Formats user inputted time into HH:MM:SS
        '''
        if self.spin_buttons[0][0].get_value_as_int() + self.spin_buttons[1][0].get_value_as_int() == 0:
            self.spin_buttons[0][0].set_value(0)
            self.spin_buttons[1][0].set_value(1)
            return '00:01:00'

        result = ""
        for time_item in self.spin_buttons[:2]:
            temp = time_item[0].get_text()
            if len(time_item[0].get_text()) == 1:
                temp = "0" + time_item[0].get_text()
            result += temp + ":"

        return result+'00'


def gtk_style():
    '''
    Function that makes hooks Gtk up to our CSS file
    Based off examples from: https://www.programcreek.com/python/example/88440/gi.repository.Gtk.CssProvider
    '''
    style_provider = Gtk.CssProvider()
    # Reads file in binary mode
    try:
        #/mnt/home/huangw46/.ssh/New_GUI/icer_gui
        #/opt/software/GUI/GreenMambaGUI/GUI.css
        with open("/mnt/home/huangw46/.ssh/New_GUI/icer_gui/GUI.css", "rb") as f:
            css = f.read()
        style_provider.load_from_data(css)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
    except:
        print("Failed to load CSS file")

class MsgDialog(Gtk.Dialog):
    def __init__(self, parent, title="New message window"):
        Gtk.Dialog.__init__(self, title, parent, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)
        self.content = Gtk.Label("This is a dialog to display additional information")
        box = self.get_content_area()
        box.add(self.content)
        self.show_all()

    def set_content(self, content):
        self.content.set_text(content)



class SubmitJobWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Submit Job")
        self.set_default_size(500, 100)

        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(50)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        self.submit_job_label = Gtk.Label()

        # Set up the file picker
        file_picker = Gtk.Button.new_with_label("Select Job To Submit")
        file_picker.connect("clicked", self.on_file_clicked)
        self.job_to_submit = None
        self.folder = None

        submit_button = Gtk.Button.new_with_label("Submit Job")
        submit_button.connect("clicked", self.submit_click)

        self.grid.attach(file_picker, 0, 1, 1, 1)
        self.grid.attach(self.submit_job_label, 1, 1, 1, 1)
        self.grid.attach(submit_button, 0, 5, 1, 1)

    def on_file_clicked(self, widget):
        dialog = Gtk.FileChooserDialog("Please choose a file", self,
                                       Gtk.FileChooserAction.OPEN,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                        Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.job_to_submit = dialog.get_filename()
            self.submit_job_label.set_text(self.job_to_submit)
            # print(self.job_to_submit)

        dialog.destroy()

    def submit_click(self, widget):
        if (self.job_to_submit != None):
            slash_ndx = self.job_to_submit.rfind("/", 0, len(self.job_to_submit))
            if slash_ndx != -1:
                self.folder = self.job_to_submit[0:slash_ndx]

            p = subprocess.Popen(['sbatch', '-D', self.folder, self.job_to_submit])



def create_node_window(server_name, q):
    print("Setting up node window")
    node_window = DisplayWindow(960, 600, title="Node Info")
    node_info_command = "./Test | awk '{print $1 \",\" $2 \",\" $3 \",\" $4 \",\" $5 \",\" \
             $6 \",\" $7 \",\" $8 \",\" substr($9,10) }' "

    node_window.set_listmodel(Gtk.ListStore(str, str, str, str, str, str, str, str))
    node_window.real_window(command=node_info_command, nodes_detail='general')
    q.put(node_window)

def create_job_window(server_name, q):
    print("Setting up job window")
    job_window = DisplayWindow(960, 600, title="Job Info")
    job_command = "qs -a | sed -n '1,2!p' | awk '{print}'"
    job_window.set_listmodel(Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str))
    job_window.real_window(command=job_command, nodes_detail="job_info")
    q.put(job_window)

'''def update_node_status():
    timer = threading.Timer(15.0, update_node_status)
    timer.start()'''


task_id = os.getpid()
stop_thread = False
gtk_style()
node_queue = queue.Queue()
job_queue = queue.Queue()
#t = Thread(target=create_node_window, args=("server01",q))

win = MainWindow()
win.set_process_id(task_id)
win.add_task(Thread(target=create_node_window, args=("server01",node_queue)), "node_window")
win.add_task(Thread(target=create_job_window, args=("server01",job_queue)), "job_window")
win.set_node_queue(node_queue)
win.set_job_queue(job_queue)

win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
